package net.jasonstone.deaththing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public final class DeathThing extends JavaPlugin implements Listener {
	public Logger logger;
	public HashMap<String, Stack<ItemStack>> inventories;

	public Material[] savedTypes;

	@Override
	public void onEnable() {
		logger = getLogger();

		savedTypes = new Material[] { Material.DIAMOND_AXE, Material.DIAMOND_BOOTS, Material.DIAMOND_CHESTPLATE,
				Material.DIAMOND_HELMET, Material.DIAMOND_HOE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_PICKAXE,
				Material.DIAMOND_SHOVEL, Material.DIAMOND_SWORD, Material.NETHERITE_AXE, Material.NETHERITE_BOOTS,
				Material.NETHERITE_CHESTPLATE, Material.NETHERITE_HELMET, Material.NETHERITE_HOE,
				Material.NETHERITE_LEGGINGS, Material.NETHERITE_PICKAXE, Material.NETHERITE_SHOVEL,
				Material.NETHERITE_SWORD, Material.ELYTRA };

		getServer().getPluginManager().registerEvents(this, this);
		inventories = new HashMap<String, Stack<ItemStack>>();
	}

	@EventHandler
	void someoneLoggedIn(PlayerLoginEvent event) {
		String uuid = event.getPlayer().getUniqueId().toString();
		String playerName = event.getPlayer().getDisplayName();

		if (!inventories.containsKey(uuid)) {
			logger.info(String.format("Creating death storage for %s (%s)", playerName, uuid));
			inventories.put(uuid, new Stack<ItemStack>());
		}
	}

	@EventHandler
	public void someoneDied(PlayerDeathEvent event) {
		String uuid = event.getEntity().getUniqueId().toString();
		List<ItemStack> drops = event.getDrops();

		Iterator<ItemStack> i = drops.iterator();

		while (i.hasNext()) {
			ItemStack stack = i.next();
			logger.info(stack.toString());
			Material type = stack.getType();

			if (Arrays.asList(savedTypes).contains(type)) {
				inventories.get(uuid).push(stack);
				i.remove();
			}

		}
	}

	@EventHandler
	public void someoneCameBack(PlayerRespawnEvent event) {
		String uuid = event.getPlayer().getUniqueId().toString();

		Stack<ItemStack> stuff = inventories.get(uuid);

		while (!stuff.isEmpty()) {
			ItemStack stack = stuff.pop();
			logger.info(stack.toString());
			event.getPlayer().getInventory().addItem(stack);
		}

	}
}
